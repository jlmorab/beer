package com.example.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BeerSelect extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{
		
		String c = (String)req.getParameter("color");
		System.out.println(c);
		req.setAttribute("style", c);
		
		RequestDispatcher dispatcher= req.getRequestDispatcher("result.jsp");
		dispatcher.forward(req, resp);
	}
	
}
